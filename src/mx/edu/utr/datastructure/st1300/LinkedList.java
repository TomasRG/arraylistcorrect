/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructure.st1300;

import java.awt.font.NumericShaper.Range;
import mx.edu.utr.datastructures.List;

/**
 *
 * @author tomye
 */
public class LinkedList implements List {

    class Node {

        Object element;
        Node Previous;
        Node Next;

        public Node(Object element, Node Previous, Node Next) {

        }
    }
    int size;
    Node head;
    Node tail;

    public LinkedList() {
        this.head = new Node(null, null, null);
        this.tail = new Node(null, this.head, null);
        this.head.Next = this.tail;
    }

    private void addBefore(Object element, Node tail) {
        size++;
    }

    private Node getNode(int index) {
        return null;
    }

    @Override
    public boolean add(Object element) {
        addBefore(element, tail);
        return true;
    }

    @Override
    public void add(int index, Object element) {
        addBefore(element, getNode(index));
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object get(int index) {
        return getNode(index).element;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object remove(int index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object set(int index, Object element) {
        Node n = getNode(index);
        Object old = n.element;
        n.element = element;
        return old;
    }

    @Override
    public int size() {
        return size;
    }

}
